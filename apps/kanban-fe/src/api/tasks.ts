import { axiosClient } from '../utils/axiosClient';

export const createTask = async (
  name: string,
  description: string,
  category: number
) => {
  const response = await axiosClient.post('/task', {
    category,
    description,
    name,
  });
  return response.data;
};

export const deleteTask = async (taskId: number) => {
  const response = await axiosClient.delete(`/task/${taskId}`);
  return response.data;
};

export const dropTask = async (taskId: number, newCategoryId: number) => {
  const response = await axiosClient.patch(`/task/${taskId}`, {
    category: newCategoryId,
  });
  return response.data;
};

export const getTasks = async () => {
  const response = await axiosClient.get('/task');
  return response.data;
};

export const updateTask = async (
  taskId: number,
  name: string,
  description: string,
  category: number
) => {
  const response = await axiosClient.patch(`/task/${taskId}`, {
    category,
    description,
    name,
  });
  return response.data;
};
