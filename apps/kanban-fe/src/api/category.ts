import { axiosClient } from '../utils/axiosClient';

export const createCategory = async (name: string) => {
  const response = await axiosClient.post('/category', { name });
  return response.data;
};

export const deleteCategory = async (id: number) => {
  const response = await axiosClient.delete(`/category/${id}`);
  return response.data;
};

export const getCategories = async () => {
  const response = await axiosClient.get('/category');
  return response.data;
};

export const updateCategory = async (id: number, name: string) => {
  const response = await axiosClient.put(`/category/${id}`, { name });
  return response.data;
};
