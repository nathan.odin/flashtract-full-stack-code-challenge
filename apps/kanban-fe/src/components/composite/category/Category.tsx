import React from 'react';
import { Box, Center, Heading, HStack, useToast } from '@chakra-ui/react';
import { AddIcon, DeleteIcon } from '@chakra-ui/icons';

import Button from '../../simple/button/Button';
import CategoryList from './CategoryList';

import { deleteCategory } from '../../../api';

import { CategoryType } from './Category.type';
import { TaskType } from '../task/task.type';

const Category = ({
  category,
  categories,
  fetchCategories,
  fetchTasks,
  onOpen,
  setSelectedCategory,
  tasks,
}: {
  category: CategoryType;
  categories: any;
  fetchCategories: () => void;
  fetchTasks: () => void;
  onOpen: () => void;
  setSelectedCategory: (category: number) => void;
  tasks: { [key: number]: TaskType[] };
}) => {
  const toast = useToast();

  return (
    <Box
      bg="white"
      borderWidth="1px"
      borderRadius="lg"
      key={category.id}
      p={5}
      overflow="hidden"
      w="100%"
    >
      <Center>
        <HStack spacing={4}>
          <Heading as={'h3'} size={'md'}>
            {category.name}
          </Heading>
          <DeleteIcon
            color={'red'}
            onClick={async () => {
              await deleteCategory(category.id);
              toast({
                title: 'Category deleted.',
                description: `${category.name} category has been deleted successfully.`,
                status: 'success',
                duration: 9000,
                isClosable: true,
              });
              fetchCategories();
            }}
          />
        </HStack>
      </Center>
      <CategoryList
        category={category}
        categories={categories}
        fetchTasks={fetchTasks}
        tasks={tasks}
      />
      <Center>
        <Button
          leftIcon={<AddIcon />}
          label="Add"
          onClick={() => {
            setSelectedCategory(category.id);
            onOpen();
          }}
          outline
        />
      </Center>
    </Box>
  );
};

export default Category;
