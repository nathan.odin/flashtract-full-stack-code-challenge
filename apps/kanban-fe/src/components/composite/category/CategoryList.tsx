import React from 'react';
import { Droppable } from 'react-beautiful-dnd';
import { Stack } from '@chakra-ui/react';

import TaskListItem from '../task/TaskListItem';

import { TaskType } from '../task/task.type';
import { CategoryType } from './Category.type';

const CategoryList = ({
  category,
  categories,
  fetchTasks,
  tasks,
}: {
  category: CategoryType;
  categories: [];
  fetchTasks: () => void;
  tasks: { [key: number]: TaskType[] };
}) => {
  return (
    <Droppable droppableId={category.id.toString()}>
      {(provided, snapshot) => (
        <Stack spacing={4} m={5} ref={provided.innerRef}>
          {tasks[category.id] && (
            <div>
              {tasks[category.id].map(
                (
                  task: {
                    id: number;
                    name: string;
                    description: string;
                    createdAt: Date;
                    updatedAt: Date;
                    category: number;
                  },
                  idx: number
                ) => (
                  <TaskListItem
                    categories={categories}
                    fetchTasks={fetchTasks}
                    idx={idx}
                    task={task}
                  />
                )
              )}
            </div>
          )}
        </Stack>
      )}
    </Droppable>
  );
};

export default CategoryList;
