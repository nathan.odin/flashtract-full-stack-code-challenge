import React from 'react';
import {
  Button,
  Center,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Box,
} from '@chakra-ui/react';

import { TaskType } from './task.type';

const TaskInfo = ({
  task,
  isOpen,
  onClose,
}: {
  task: TaskType;
  isOpen: boolean;
  onClose: () => void;
}) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>{task.name}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <label>Description:</label>
          <p>{task.description}</p>
          <Box mt={5}>
            <p>Created At: {task.createdAt}</p>
            <p>Updated At: {task.updatedAt}</p>
          </Box>
        </ModalBody>

        <Center>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </Center>
      </ModalContent>
    </Modal>
  );
};

export default TaskInfo;
