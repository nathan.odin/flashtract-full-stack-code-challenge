import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import {
  Box,
  Heading,
  HStack,
  useDisclosure,
  useToast,
} from '@chakra-ui/react';

import {
  ArrowLeftIcon,
  ArrowRightIcon,
  DeleteIcon,
  DragHandleIcon,
} from '@chakra-ui/icons';

import { deleteTask, updateTask } from '../../../api';
import { TaskType } from './task.type';
import TaskInfo from './TaskInfo';

const TaskListItem = ({
  categories,
  fetchTasks,
  idx,
  task,
}: {
  categories: [];
  fetchTasks: () => void;
  idx: number;
  task: TaskType;
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();

  return (
    <>
      <Draggable key={task.id} draggableId={task.id.toString()} index={idx}>
        {(provided, snapshot) => (
          <Box
            bg="white"
            borderWidth="1px"
            borderRadius="lg"
            shadow="md"
            my={5}
            p={3}
            ref={provided.innerRef}
            {...provided.draggableProps}
          >
            <HStack spacing={4} justifyContent={'space-between'}>
              <HStack spacing={4}>
                <div {...provided.dragHandleProps}>
                  <DragHandleIcon />
                </div>

                <Heading
                  as={'h4'}
                  size={'sm'}
                  onClick={onOpen}
                  cursor="pointer"
                >
                  {task.name}
                </Heading>
              </HStack>
              <HStack spacing={4}>
                <ArrowLeftIcon
                  onClick={async () => {
                    const categoryIndex = categories.findIndex(
                      (e: any) => e.id === task.category
                    );
                    if (categoryIndex === 0) return;

                    const newCategory: any = categories[categoryIndex - 1];

                    await updateTask(
                      task.id,
                      task.name,
                      task.description,
                      newCategory.id
                    );
                    toast({
                      title: 'Task moved.',
                      description: `${task.name} task has been moved successfully.`,
                      status: 'success',
                      duration: 9000,
                      isClosable: true,
                    });
                    fetchTasks();
                  }}
                />
                <ArrowRightIcon
                  onClick={async () => {
                    const categoryIndex = categories.findIndex(
                      (e: any) => e.id === task.category
                    );
                    if (categoryIndex === categories.length - 1) return;

                    const newCategory: any = categories[categoryIndex + 1];

                    await updateTask(
                      task.id,
                      task.name,
                      task.description,
                      newCategory.id
                    );
                    toast({
                      title: 'Task moved.',
                      description: `${task.name} task has been moved successfully.`,
                      status: 'success',
                      duration: 9000,
                      isClosable: true,
                    });
                    fetchTasks();
                  }}
                />
                <DeleteIcon
                  color={'red'}
                  onClick={async () => {
                    await deleteTask(task.id);
                    toast({
                      title: 'Task deleted.',
                      description: `${task.name} task has been deleted successfully.`,
                      status: 'success',
                      duration: 9000,
                      isClosable: true,
                    });
                    fetchTasks();
                  }}
                />
              </HStack>
            </HStack>
          </Box>
        )}
      </Draggable>
      <TaskInfo task={task} isOpen={isOpen} onClose={onClose} />
    </>
  );
};

export default TaskListItem;
