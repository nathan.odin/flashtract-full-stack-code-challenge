export type TaskType = {
  id: number;
  name: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  category: number;
};
