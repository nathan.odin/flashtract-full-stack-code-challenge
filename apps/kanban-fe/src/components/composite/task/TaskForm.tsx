import React, { useState } from 'react';
import {
  Button,
  Center,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useToast,
} from '@chakra-ui/react';

import Input from '../../simple/input/Input';
import { createTask } from '../../../api';

const TaskForm = ({
  category,
  fetchTasks,
  isOpen,
  onClose,
}: {
  category: number;
  fetchTasks: () => void;
  isOpen: boolean;
  onClose: () => void;
}) => {
  const toast = useToast();
  const [newTaskName, setNewTaskName] = useState('');
  const [newTaskDescription, setNewTaskDescription] = useState('');

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Create Task</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <label>Name</label>
          <Input
            name="name"
            onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
              setNewTaskName(e.target.value)
            }
            placeholder="Name"
            value={newTaskName}
          />
          <label>Description</label>
          <Input
            name="description"
            onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
              setNewTaskDescription(e.target.value)
            }
            placeholder="Description"
            value={newTaskDescription}
          />
        </ModalBody>

        <Center>
          <ModalFooter>
            <Button
              colorScheme="green"
              mr={3}
              onClick={async () => {
                await createTask(newTaskName, newTaskDescription, category);
                setNewTaskName('');
                setNewTaskDescription('');
                toast({
                  title: 'Task created.',
                  description: `${newTaskName} task has been created successfully.`,
                  status: 'success',
                  duration: 9000,
                  isClosable: true,
                });
                fetchTasks();
                onClose();
              }}
            >
              Save
            </Button>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Close
            </Button>
          </ModalFooter>
        </Center>
      </ModalContent>
    </Modal>
  );
};

export default TaskForm;
