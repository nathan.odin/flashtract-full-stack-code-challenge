import React, { useState } from 'react';
import { Center, Container, Heading, useToast } from '@chakra-ui/react';
import { AddIcon } from '@chakra-ui/icons';

import Button from '../../simple/button/Button';
import Input from '../../simple/input/Input';

import { createCategory } from '../../../api';

const Header = ({ fetchCategories }: { fetchCategories: () => void }) => {
  const toast = useToast();
  const [newCategoryName, setNewCategoryName] = useState('');

  return (
    <Container mb={5}>
      <Center>
        <Heading color="white" as={'h1'} size={'xl'} mb={5}>
          Kanban Board
        </Heading>
      </Center>

      <Input
        name="category"
        onChange={(e: React.ChangeEvent<HTMLInputElement>): void =>
          setNewCategoryName(e.target.value)
        }
        placeholder="New category"
        value={newCategoryName}
      />
      <Center mt={5}>
        <Button
          leftIcon={<AddIcon />}
          label="Add"
          onClick={async () => {
            await createCategory(newCategoryName);
            toast({
              title: 'Category created.',
              description: `${newCategoryName} category has been created successfully.`,
              status: 'success',
              duration: 9000,
              isClosable: true,
            });
            setNewCategoryName('');
            fetchCategories();
          }}
        />
      </Center>
    </Container>
  );
};

export default Header;
