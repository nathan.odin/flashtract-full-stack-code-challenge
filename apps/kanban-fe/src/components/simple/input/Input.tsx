import React from 'react';
import { Input as ChakraInput } from '@chakra-ui/react';

const Input = ({
  name,
  onChange,
  placeholder,
  value,
}: {
  name: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  placeholder?: string;
  value: string;
}) => {
  return (
    <ChakraInput
      bg="white"
      name={name}
      onChange={onChange}
      placeholder={placeholder}
      value={value}
    />
  );
};

export default Input;
