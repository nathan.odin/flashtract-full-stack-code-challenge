import React from 'react';
import { Button as ChakraButton } from '@chakra-ui/react';

const Button = ({
  leftIcon,
  label,
  onClick,
  outline = false,
}: {
  leftIcon?: JSX.Element;
  label: string;
  onClick: () => void;
  outline?: boolean;
}) => {
  return (
    <ChakraButton
      colorScheme="blue"
      variant={outline ? 'outline' : 'solid'}
      leftIcon={leftIcon || undefined}
      onClick={onClick}
    >
      {label}
    </ChakraButton>
  );
};

export default Button;
