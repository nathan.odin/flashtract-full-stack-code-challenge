import React, { useEffect, useState } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { Box, HStack, useDisclosure, useToast } from '@chakra-ui/react';

import { dropTask, getCategories, getTasks } from './api';

import TaskForm from './components/composite/task/TaskForm';
import Category from './components/composite/category/Category';
import Header from './components/layout/header/Header';

const App = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const toast = useToast();

  const [categories, setCategories] = useState([]);
  const [tasks, setTasks] = useState({} as { [key: number]: any });

  const [selectedCategory, setSelectedCategory] = useState(0);

  const fetchCategories = async () => {
    const categories = await getCategories();
    setCategories(categories);
  };

  const fetchTasks = async () => {
    const tasks = await getTasks();
    const tasksByCategory: any = {};
    tasks.forEach(
      (task: {
        id: number;
        name: string;
        description: string;
        createdAt: Date;
        updatedAt: Date;
        category: number;
      }) => {
        tasksByCategory[task.category]
          ? tasksByCategory[task.category].push(task)
          : (tasksByCategory[task.category] = [task]);
      }
    );
    setTasks(tasksByCategory);
  };

  useEffect(() => {
    fetchCategories();
    fetchTasks();
  }, []);

  return (
    <DragDropContext
      onDragEnd={async (props: any) => {
        await dropTask(props.draggableId, props.destination.droppableId);
        toast({
          title: 'Task moved.',
          description: `Task has been moved successfully.`,
          status: 'success',
          duration: 9000,
          isClosable: true,
        });
        fetchTasks();
      }}
    >
      <Box bg="gray" h={'100vh'}>
        <Header fetchCategories={fetchCategories} />

        {categories.length > 0 && (
          <HStack spacing={4} padding={4}>
            {categories.map((category: { id: number; name: string }) => (
              <Category
                category={category}
                categories={categories}
                fetchCategories={fetchCategories}
                fetchTasks={fetchTasks}
                onOpen={onOpen}
                setSelectedCategory={setSelectedCategory}
                tasks={tasks}
              />
            ))}
          </HStack>
        )}
      </Box>
      <TaskForm
        category={selectedCategory}
        fetchTasks={fetchTasks}
        isOpen={isOpen}
        onClose={onClose}
      />
    </DragDropContext>
  );
};

export default App;
