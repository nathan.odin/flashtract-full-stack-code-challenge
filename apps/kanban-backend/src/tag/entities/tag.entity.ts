import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Unique,
  ManyToMany,
} from 'typeorm';

import { Task } from '../../task/entities/task.entity';

@Entity()
@Unique(['name'])
export class Tag {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  // @Column()
  // @ManyToMany(() => Task, (task) => task.tags)
  // tasks: Task[];
}
