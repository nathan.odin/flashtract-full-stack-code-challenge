import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Task } from './entities/task.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private taskRepository: Repository<Task>
  ) {}

  create(createTaskDto: CreateTaskDto) {
    const date = new Date();

    const task = {
      ...createTaskDto,
      createdAt: date,
      updatedAt: date,
    };

    return this.taskRepository.save(task);
  }

  findAll() {
    return this.taskRepository.find();
  }

  findOne(id: number) {
    return this.taskRepository.findOne(id);
  }

  update(id: number, updateTaskDto: UpdateTaskDto) {
    const task = {
      ...updateTaskDto,
      updatedAt: new Date(),
    };

    return this.taskRepository.update(id, task);
  }

  remove(id: number) {
    return this.taskRepository.delete(id);
  }
}
