import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToMany,
  JoinTable,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { Category } from '../../category/entities/category.entity';

import { Tag } from '../../tag/entities/tag.entity';

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  // @Column()
  // @ManyToMany(() => Tag, (tag) => tag.tasks)
  // @JoinTable()
  // tags: Tag[];

  @Column()
  @ManyToOne(() => Category, { onDelete: 'CASCADE' })
  @JoinColumn()
  category: number;
}
