import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { CategoryModule } from '../category/category.module';
import { TagModule } from '../tag/tag.module';
import { TaskModule } from '../task/task.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      entities: ['src/**/*.entity.ts'],
      autoLoadEntities: true,
      synchronize: true,
      database: 'kanban-db.sqlite3',
      type: 'sqlite',
    }),
    CategoryModule,
    TagModule,
    TaskModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
